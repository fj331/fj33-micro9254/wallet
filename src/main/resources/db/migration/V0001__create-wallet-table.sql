CREATE TABLE wallets (
    id UUID,
    name VARCHAR(255) NOT NULL ,
    user_id UUID NOT NULL,

    CONSTRAINT pk_wallets PRIMARY KEY(id)
);