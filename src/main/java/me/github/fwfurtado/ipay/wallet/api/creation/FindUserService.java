package me.github.fwfurtado.ipay.wallet.api.creation;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;
import java.util.UUID;

@FeignClient(name = "users", decode404 = true)
public interface FindUserService {

    @GetMapping("users/{id}")
    Optional<UserView> findUserById(@PathVariable UUID id);
}
