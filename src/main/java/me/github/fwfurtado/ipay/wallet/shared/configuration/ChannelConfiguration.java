package me.github.fwfurtado.ipay.wallet.shared.configuration;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.SubscribableChannel;

@Configuration
@EnableBinding(ChannelConfiguration.UserChannel.class)
public class ChannelConfiguration {


    public static interface UserChannel {

        String CHANNEL = "user";

        @Input(CHANNEL)
        SubscribableChannel input();
    }
}
