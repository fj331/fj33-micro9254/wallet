package me.github.fwfurtado.ipay.wallet.api.creation;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class UserView {
    private List<String> emails;
}
