package me.github.fwfurtado.ipay.wallet.api.creation;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.created;

@RestController
@AllArgsConstructor
class CreationController {

    private final CreationServiceImpl service;

    @PostMapping("wallets")
    ResponseEntity<?> creationBy(@RequestBody @Valid WalletCreationForm form, UriComponentsBuilder uriBuilder) {
        var id = service.createWalletBy(form);

        var uri = uriBuilder.path("wallets/{id}").build(id);

        return created(uri).build();
    }

}
