package me.github.fwfurtado.ipay.wallet.shared.repositories;

import me.github.fwfurtado.ipay.wallet.api.creation.CreationRepository;
import me.github.fwfurtado.ipay.wallet.api.listing.MyWalletsRepository;
import me.github.fwfurtado.ipay.wallet.shared.domain.Wallet;
import org.springframework.data.repository.Repository;

import java.util.UUID;

interface WalletRepository extends Repository<Wallet, UUID>, CreationRepository, MyWalletsRepository {
}
