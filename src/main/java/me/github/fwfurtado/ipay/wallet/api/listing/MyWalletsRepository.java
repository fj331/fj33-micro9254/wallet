package me.github.fwfurtado.ipay.wallet.api.listing;

import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface MyWalletsRepository {
    @Query("select w from Wallet w where w.userId = :userId")
    List<WalletView> listWalletsOf(UUID userId);
}
