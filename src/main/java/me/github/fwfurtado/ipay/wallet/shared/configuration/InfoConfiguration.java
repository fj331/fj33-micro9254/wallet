package me.github.fwfurtado.ipay.wallet.shared.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class InfoConfiguration implements InfoContributor {

    @Value("${project.owner:undefined}")
    private String owner;

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("owner", owner);
    }
}
