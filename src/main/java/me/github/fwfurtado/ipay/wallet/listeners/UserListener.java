package me.github.fwfurtado.ipay.wallet.listeners;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import me.github.fwfurtado.ipay.wallet.api.creation.CreationService;
import me.github.fwfurtado.ipay.wallet.api.creation.WalletCreationForm;
import me.github.fwfurtado.ipay.wallet.shared.configuration.ChannelConfiguration.UserChannel;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class UserListener {


    private CreationService service;

    @Data
    @ToString
    static class CreatedUserEvent {
        private UUID id;
        private List<String> emails;
        private String password;
    }


    @StreamListener(UserChannel.CHANNEL)
    void handle(CreatedUserEvent event) {
        System.out.println(event);

        var form = new WalletCreationForm();
        form.setName("Main");
        form.setUserId(event.getId());

        service.createWalletBy(form);
    }

}
