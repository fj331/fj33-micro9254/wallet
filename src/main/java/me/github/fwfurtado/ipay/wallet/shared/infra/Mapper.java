package me.github.fwfurtado.ipay.wallet.shared.infra;

public interface Mapper<S, T> {
    T map (S source);
}
