package me.github.fwfurtado.ipay.wallet.api.creation;

import org.springframework.stereotype.Service;

import java.util.UUID;

public interface CreationService {
    UUID createWalletBy(WalletCreationForm form);
}
