package me.github.fwfurtado.ipay.wallet.api.creation;

import me.github.fwfurtado.ipay.wallet.shared.domain.Wallet;

public interface CreationRepository {
    void save(Wallet wallet);
}
