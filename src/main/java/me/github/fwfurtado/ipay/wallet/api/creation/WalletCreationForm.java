package me.github.fwfurtado.ipay.wallet.api.creation;

import lombok.Data;

import java.util.UUID;

@Data
public class WalletCreationForm {
    private UUID userId;

    private String name;
}
