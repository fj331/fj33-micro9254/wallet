package me.github.fwfurtado.ipay.wallet.api.creation;

import lombok.AllArgsConstructor;
import me.github.fwfurtado.ipay.wallet.shared.domain.Wallet;
import me.github.fwfurtado.ipay.wallet.shared.exceptions.UserNotFoundException;
import me.github.fwfurtado.ipay.wallet.shared.infra.Mapper;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.UUID;


@Service
@AllArgsConstructor
public class CreationServiceImpl implements CreationService {
    private final FindUserService findUserService;
    private final Mapper<WalletCreationForm, Wallet> mapper;
    private final CreationRepository repository;

    @Override
    public UUID createWalletBy(WalletCreationForm form) {

        var userId = form.getUserId();
        var userNotExists = findUserService.findUserById(userId).isEmpty();

        if (userNotExists) {
            throw new UserNotFoundException(MessageFormat.format("Cannot find user with id {0}", userId));
        }

        var wallet = mapper.map(form);

        repository.save(wallet);

        return wallet.getId();
    }
}
