package me.github.fwfurtado.ipay.wallet.api.listing;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@AllArgsConstructor
class MyWalletsController {

    private final MyWalletsRepository repository;

    @GetMapping("wallets/me")
    ResponseEntity<?> showWalletBy(String userId) {

        System.out.println("REQUEST");

        var wallets = repository.listWalletsOf(UUID.fromString(userId));

        if (wallets.isEmpty()) {
            return notFound().build();
        }

        return ok(WalletWrapper.of(wallets));
    }

    @Data
    @AllArgsConstructor
    static private class WalletWrapper {
        private List<WalletView> wallets;

        private static WalletWrapper of(List<WalletView> wallets) {
            return new WalletWrapper(wallets);
        }
    }
}
