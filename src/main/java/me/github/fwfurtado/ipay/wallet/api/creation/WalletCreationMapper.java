package me.github.fwfurtado.ipay.wallet.api.creation;

import me.github.fwfurtado.ipay.wallet.shared.domain.Wallet;
import me.github.fwfurtado.ipay.wallet.shared.infra.Mapper;
import org.springframework.stereotype.Component;

@Component
class WalletCreationMapper implements Mapper<WalletCreationForm, Wallet> {
    @Override
    public Wallet map(WalletCreationForm source) {
        return new Wallet(source.getUserId(), source.getName());
    }
}
