package me.github.fwfurtado.ipay.wallet.listeners;

import me.github.fwfurtado.ipay.wallet.WalletApplication;
import me.github.fwfurtado.ipay.wallet.api.creation.CreationServiceImpl;
import me.github.fwfurtado.ipay.wallet.api.creation.WalletCreationForm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = WalletApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureStubRunner(ids = "me.github.fwfurtado.ipay:user:+:stubs:7774", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class UserListenerTest {

    @Autowired
    private StubTrigger trigger;

    @MockBean
    private CreationServiceImpl service;

    @Captor
    private ArgumentCaptor<WalletCreationForm> formArgumentCaptor;

    @Test
    void shouldReceiveNewUser() {
        trigger.trigger("new-user");

        then(service).should().createWalletBy(formArgumentCaptor.capture());

        var form = formArgumentCaptor.getValue();
        var userId = form.getUserId();

        assertEquals(UUID.fromString("30806ec1-0e77-426f-9d05-9ed8fabd64c2"), userId);
    }

}