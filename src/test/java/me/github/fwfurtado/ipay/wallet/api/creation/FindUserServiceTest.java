package me.github.fwfurtado.ipay.wallet.api.creation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@AutoConfigureStubRunner(ids = "me.github.fwfurtado.ipay:user:+:stubs:7774", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class FindUserServiceTest {


    private RestTemplate rest;

    @BeforeEach
    void setup() {
        rest = new RestTemplate();
    }

    @Test
    void shouldGetAnUserByValidId() {
        var user = rest.getForObject("http://localhost:7774/users/9925923a-43b0-41f0-a897-89dcbfd790a9?pages=1", UserView.class);

        System.out.println(user);
    }

    @Test
    void shouldReturn404WhenIdIsInvalid() {
        try {
            rest.getForObject("http://localhost:7774/users/30806ec1-0e77-426f-9d05-9ed8fabd64c2?pages=1", UserView.class);
        } catch (HttpClientErrorException e) {
            var status = e.getStatusCode();

            assertEquals(HttpStatus.NOT_FOUND, status);
        }

    }
}